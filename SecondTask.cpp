#include <iostream>
#include <math.h>
#include <cmath>
#include <vector>
#include <errno.h>
#include <fstream>
#include <iomanip>
using namespace std;

void logarithm_of_number(double, double, double, double, double);
ofstream fout;


int main(){

	fout.open("LogTable2.txt");
	fout << "LowestNumber\tHighestNumber\tnewNumber\t\tlogLowest\tlogHighest\tLogNewNumber" << endl;

	cout << "Enter a number: ";
	double num;
	cin >> num;
	logarithm_of_number(num, 100, 1000, 2, 3);

		
	return 0;
}


void logarithm_of_number(double target, double min, double max, double minLog, double maxLog){
	double K = min*max;
	double logK = (minLog + maxLog)/2.0;

	
	if (abs(minLog - maxLog) <= 0.000001){
		cout << "Log of " << target << " is: " << minLog << endl;
		return;		
	}
   
   		
	fout << setw(15) << setprecision(10) << min<< "\t";
	fout << setw(15)<< setprecision(10) << max << "\t";
	fout << setw(15)<< setprecision(10) << K << "\t";
	fout << setw(15)<< setprecision(10) << minLog << "\t";
	fout << setw(15)<< setprecision(10) << maxLog << "\t";
	fout << setw(15)<< setprecision(10) << logK << endl;
   
    if (target < sqrt(K)){
		logarithm_of_number(target, min, sqrt(K), minLog, logK);
	}
	else if (target > sqrt(K)){
		logarithm_of_number(target, sqrt(K), max, logK, maxLog);
	}	
				
	
}


